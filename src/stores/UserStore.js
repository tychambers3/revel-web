import { observable, action, decorate, reaction } from 'mobx';

class UserStore {
  constructor() {
    reaction(
      () => this.user,
      user => window.sessionStorage.setItem('user', JSON.stringify(user))
    );
  }

  user = {} 

  setUser(user) {
    this.user = {...user};
  }

  getUser() {
    return JSON.parse(window.sessionStorage.getItem('user'));
  }

  setToken(token) {
    window.localStorage.setItem('token', token);
  }

  getToken() {
    return window.localStorage.getItem('token');
  }

  getFullName() {
    const user = JSON.parse(window.sessionStorage.getItem('user'));

    return user.usernameOnly ? user.username : `${user.firstName} ${user.lastName}`;
  }
}

decorate(UserStore, {
  user: observable,
  setUser: action
});
export default new UserStore();