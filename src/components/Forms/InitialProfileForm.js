import React, { useState } from 'react';
import { InputField, SelectGender, SliderInput, DatePicker, SelectField, TextareaField } from '../../components/Inputs';
import { religions, education, smoke, drink, relationshipType, ethnicity } from '../../constants/options';
import Axios from 'axios';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react-lite';

const InitialProfileForm = (props) => {

  const [ location, setLocation ] = useState("");
  const [ locationCoords, setCoords ] = useState({});
  const [ firstName, setFirstName ] = useState("");
  const [ lastName, setLastName ] = useState('');
  const [username, setUsername] = useState("");
  const [gender, setGender] = useState("");
  const [height, setHeight] = useState(122);
  const [age, setAge] = useState(0);
  const [religion, setReligion] = useState("");
  const [job, setJob] = useState("");
  const [ getEthnicity, setEthnicity ] = useState("");
  const [ getSmoke, setSmoke ] = useState("");
  const [ getDrink, setDrink ] = useState("");
  const [ getRelationshipType, setRelationshipType ] = useState("");
  const [ usernameOnly, setUsernameOnly ] = useState(false);
  const [ personalSummary, setPersonalSummary ] = useState("");
  const properties = {
    firstName,
    lastName,
    username,
    gender,
    height,
    religion,
    age,
    job,
    ethnicity: getEthnicity,
    smoke: getSmoke,
    drink: getDrink,
    relationshipType: getRelationshipType,
    usernameOnly,
    personalSummary,
    locationCoords
  }
  return (
    
    <form action="" className="form">
        <InputField 
          placeholder="First Name"
          type="text"
          name="firstName"
          label="First Name"
          onChange={(e) => setFirstName(e.target.value)}
        />

        <InputField 
          placeholder="Last Name"
          type="text"
          name="lastName"
          label="Last Name"
          onChange={(e) => setLastName(e.target.value)}
        />
        
        <InputField 
          placeholder="Username"
          type="text"
          name="username"
          label="Username"
          style={{marginBottom: 0}}
          onChange={(e) => setUsername(e.target.value)}
        />
        <div className="d-f ai-c">
          <input type="checkbox" className="mr-" onChange={e => setUsernameOnly(e.target.checked)}/>
          <p className="subtle">Use username rather than first and last name (keeps some info private)</p>
        </div>

        <InputField 
          placeholder="Occupation"
          type="text"
          name="occupation"
          label="What do you do for a living?"
          onChange={(e) => setJob(e.target.value)}
          style={{marginTop: '1em'}}
        />
        <InputField 
          placeholder="Age"
          type="number"
          name="age"
          label="How old are you?"
          onChange={(e) => setAge(e.target.value)}
        />

        <div className="d-f jc-sb">
          <SelectGender 
            type="checkbox"
            label="Male"
            name="male"
            onChange={e => setGender(e.target.getAttribute('name'))}
          />

          <SelectGender 
            type="checkbox"
            label="Female"
            name="female"
            onChange={e => setGender(e.target.getAttribute('name'))}
          />

          <SelectGender 
            type="checkbox"
            label="Other"
            name="other"
            onChange={e => setGender(e.target.getAttribute('name'))}
          />
        </div>

        <h3 className="ta-c mb-">How tall are you?</h3>
        <SliderInput 
          min={122}
          max={226}
          name="height"
          value={height}
          onChange={e=> setHeight(e.target.value)}
        />
        
        <SelectField 
          label="Are you part of a religion?"
          name="religion"
          dataSet={religions}
          onChange={e => setReligion(e.target.value)}
        />

        <SelectField 
          label="What's your ethnicity?"
          name="ethnicity"
          dataSet={ethnicity}
          onChange={e => setEthnicity(e.target.value)}
        />

        <SelectField 
          label="Do you smoke?"
          name="smoker"
          dataSet={smoke}
          onChange={e => setSmoke(e.target.value)}
        />

        <SelectField 
          label="How about drink?"
          name="drinker"
          dataSet={drink}
          onChange={e => setDrink(e.target.value)}
        />

        <SelectField 
          label="Now, what are you looking for?"
          name="relationshipType"
          dataSet={relationshipType}
          onChange={e => setRelationshipType(e.target.value)}
        />

        <h3 className="ta-c mb-">Where are you located?</h3>
        <i className="fas fa-map-marker-alt gps-locator ml-a mr-a" onClick={() => {
          setLocation("Getting your current location. Make sure to click allow inside your browser to use your location.");
          getLocation(setLocation, setCoords);
        }}></i>
        <p className="subtle ta-c">{location}</p>

        <h3 className="ta-c mb- mt+">Now let's wrap up the important stuff...</h3>
        <p className="subtle ta-c">Tells us a little bit more about yourself. You can say whatever you think would show off your personality the best in 500 characters or less.</p>

        <TextareaField 
          placeholder="Let's hear about yourself..."
          onChange={e => setPersonalSummary(e.target.value)}
        />

        <button className="btn btn-green ml-a mr-a mt+ mb+" onClick={e => submitHandler(e, props, properties)}>Start Matching</button>
      </form>

  );
}

const submitHandler = (e, props, properties) => {
  e.preventDefault();

  Axios.put(`${process.env.REACT_APP_DATABASE_URL}/api/user/update`, {
    ...properties
  },
  {
    headers: {
      'token': props.UserStore.getToken()
    }
  })
  .then(res => {
    props.UserStore.setUser(res.data.user);
    window.location.pathname = "/dashboard/home";
  })
  .catch(err => console.log(err));
}

const getLocation = (setLocation, setCoords) => {
  navigator.geolocation.getCurrentPosition(async position => {
    await Axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyCBNc5YK79uyp2bj-WZGgwE4iXYnpmFRwM`)
        .then(res => {
          let city = res.data.results[0].address_components[2].long_name;
          let province = res.data.results[0].address_components[4].long_name;
          let country = res.data.results[0].address_components[5].long_name;
          let long = position.coords.longitude;
          let lat = position.coords.latitude;

          const location = `${city}, ${province}, ${country}`;

          setLocation(location);
          setCoords({long, lat});
        })
        .catch(err => console.log(err));

  },
  error => alert(error.message),
    {
      enableHighAccuracy: true, timeout: 20000
    }
  );
}
export default inject("UserStore")(observer(InitialProfileForm));
