import React, { useState } from 'react'
import { InputField } from '../Inputs';
import Axios from 'axios';
import { inject, observer } from 'mobx-react';

const Form = (props) => {
  const [ email, setEmail ] = useState("");
  const [ password, setPassword ] = useState("");

  return (
    <form className="form d-f fxd-c jc-c">
      <InputField 
        placeholder="user@example.com"
        type="email"
        name="email"
        label="Email"
        iconName="fas fa-at"
        onChange={e => setEmail(e.target.value)}
      />

      <InputField 
        placeholder="Enter your password"
        type="password"
        name="password"
        label="Password"
        iconName="fas fa-lock"
        onChange={e => setPassword(e.target.value)}
      />

      <InputField 
        placeholder="Confirm your password"
        type="password"
        name="confirmPassword"
        label="Confirm Password"
        iconName="fas fa-lock"
        
      />

      <button className="btn btn-green mt- ml-a mr-a" onClick={(e) => submitHandler(e, email, password, props)}>Find your match</button>
    </form>
  )
}

const submitHandler = (e, email, password, props) => {
  e.preventDefault();
  const creds = {
    email,
    password
  }

  
  Axios.post(`${process.env.REACT_APP_DATABASE_URL}/api/auth/signup`, {
    ...creds
  })
  .then(res => {
    props.UserStore.setUser(res.data.user);
    props.UserStore.setToken(res.data.token);
    window.location.pathname = "/profile/initial";
  })
  .catch(err => console.log(err));
}

const SignupForm = inject('UserStore')(observer(Form));

export default SignupForm;
