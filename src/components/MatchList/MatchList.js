import React from 'react';
import './MatchList.scss';
const matches = require('./match.json');

export const MatchList = (props) => {
  return (
    <React.Fragment>
      <h1>Your Matches</h1>
      <div className="d-f ai-c matches-wrapper">
        <i className="fas fa-chevron-left" id="scroll-left"></i>
        <div className="matches-grid w-100pr jc-sb">
          {matches.matches.map((x, id) => {
            return (
              <div key={id} className="matches-grid-item" style={{
                background: `url(${x.profileImg}) center no-repeat`,
                backgroundSize: "cover"
              }}>
                <div className="grid-details d-f jc-sb ai-c">
                  <h4>{x.firstName}</h4>
                  <p className="subtle">{x.age}</p>
                </div>
              </div>
            );
          })}
        </div>
        <i className="fas fa-chevron-right" id="scroll-right"></i>
      </div>
    </React.Fragment>
  );
};
