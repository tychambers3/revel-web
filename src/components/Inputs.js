import React, {useState} from 'react';
import '../assets/styles/inputs.scss';

export const InputField = props => {

  return (
    <div className={`field-group`} {...props}>
      <label htmlFor={props.name} className="label">{props.label}</label>
      <div className="d-f input-wrapper light">
        {props.iconname &&
          <i className={`${props.iconName} mr- icon-input`}></i>
        }
        <input className="input" placeholder={props.placeholder} type={props.type} name={props.name}  onChange={props.onChange}/>
      </div>
    </div>
  );
}

export const SelectGender = (props) => {
  return (
    <div className="field-group d-f fxd-c ai-c radio-btn big" onClick={props.checkboxhandler}>
      <p className="label">{props.label}</p>
      <input id={props.name} className="input checkbox-styled" placeholder={props.placeholder} type={props.type} name={props.name}  {...props}/>
      <label htmlFor={props.name} className="checkbox-label"></label>
    </div>
  );
}

export const SliderInput = (props) => {
  const heightConverter = () => {
    const val = props.value;
    const realFeet = ((val*0.393700) / 12);
    const feet = Math.floor(realFeet);
    const inches = Math.round((realFeet - feet) * 12);
  
    return val + "cm " + "(" + feet + "' " + inches + `")`;
  }
  
  return (
    <div className="slider-wrapper">
      <input type="range" min={props.min} max={props.max} value={props.value} name={props.name} className="slider" onChange={props.onChange}/>
      {props.name === "height" &&
        <p className="subtle ta-c mt+">{heightConverter(props.height)}</p>
      }
    </div>
  );
};

export const DatePicker = (props) => {
  return (
    <div className="field-group">
      <label htmlFor={props.name} className="label">{props.label}</label>
      <input type="date" className="date-picker" name={props.name} onChange={(e) => console.log(e.target.value)}/>
    </div>
  );
}

export const SelectField = (props) => {
  return(
    <div className="field-group">
      <label htmlFor={props.name} className="label">{props.label}</label>
      <select className="select-field" onChange={props.onChange}>
        <option selected disabled>Select an option...</option>
        {props.dataSet.map((x, id) => {
          return(
            <option key={id} value={x.value}>
              {x.value}
            </option>
          );
        })}
      </select>
    </div>
  );
}

export const TextareaField = (props) => {
  return(
    <textarea rows={10} className="textarea mt+" placeholder={props.placeholder} {...props}/>
  );
}





