import React from 'react'
import './SignupPage.scss';
import Navbar from '../../layouts/Navbar/Navbar';
import SignupForm from '../../components/Forms/SignupForm';

const SignupPage = () => {
  return (
    <div className="signup-page pos-r">
    <Navbar />
      <div className="signup-page-content-wrapper d-f w-960px">
        <div className="signup-info-pane p-">
          <h1 className="ta-c">Features &amp; Pricing</h1>
          
          <ul>
            <li className="p-">
              <i className="fas mr+ fa-comment-alt"></i>
              <p>Send up to 100 messages to any of your matches</p>
            </li>

            <li className="p-">
              <i className="fas mr+ fa-user-clock"></i>
              <p>See who likes you for up to 24hrs</p>
            </li>

            <li className="p-">
              <i className="fas mr+ fa-video"></i>
              <p>Add a video intoduction to your profile</p>
            </li>

            <li className="p-">
              <i className="fas mr+ fa-angle-double-up"></i>
              <p>Get even more for 19.99/month (USD)</p>
            </li>
          </ul>
        </div>
        <div className="signup-page-content d-f fxd-c ai-c">
          <h1>Create your Revel account</h1>
          
          <SignupForm />
          <p className="mt+ p+ ta-c">By signing up, you agree to our <a href="">terms of service</a> and <a href="">privacy policy</a>.</p>
        </div>
      </div>
    </div>
  )
}

export default SignupPage

