import React from 'react';
import { Header } from '../../layouts/Header/Header';
import './DashboardPage.scss';
import { MatchList } from '../../components/MatchList/MatchList';
import SubNav from '../../layouts/SubNav/SubNav';
import DashboardHome from '../DashboardHome/DashboardHome';
import { MatchShowPage } from '../MatchShowPage/MatchShowPage';

export const DashboardPage = (props) => {
  const slug = props.match.params.dashboard_page;
  return(
    <div className="d-f h-100v ">
      <Header />

      <section className="main d-f fxd-c">
        <SubNav/>

        <section className="template-wrapper">
          <Templates slug={slug}/>
        </section>
      </section>
    </div>
  );

}

const Templates = props => {
  const slug = props.slug;

  const templates = {
    'home': <DashboardHome />,
    'matches': <MatchShowPage />
  }

  return templates[slug];
}