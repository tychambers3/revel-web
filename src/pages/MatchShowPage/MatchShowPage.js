import React from 'react';
import './MatchShowPage.scss';
const match = require('../../components/MatchList/match.json');

export const MatchShowPage = (props) => {
  const fMatch = match.matches[0];
  return(
    <div className="match-wrapper w-100pr d-f ai-c jc-c">
      <div className="d-f w-1056px">
        <div className="profile-img-wrapper">
          <img src={fMatch.profileImg} alt="Profile picture" />
        </div>

        <section className="profile-main-wrapper">
          <h1 className="profile-fullname">{fMatch.firstName} {fMatch.lastName}</h1>
          <span className="d-f ai-c profile-minor-details">
            <p className="subtle mr+">{fMatch.age} years old</p>
            <p className="subtle">{fMatch.location}</p>
          </span>
          <p className="subtle profile-summary">{fMatch.summary}</p>

          <ul className="d-f profile-detail-list">
            <li>
              <h5>Height</h5>
              <p>{fMatch.height}</p>
            </li>

            <li>
              <h5>Do they smoke?</h5>
              <p>{fMatch.smoke}</p>
            </li>

            <li>
              <h5>How about drink?</h5>
              <p>{fMatch.drink}</p>
            </li>

            <li>
              <h5>Religion</h5>
              <p>{fMatch.religion}</p>
            </li>

            <li>
              <h5>Job</h5>
              <p>{fMatch.job}</p>
            </li>

            <li>
              <h5>Education</h5>
              <p>{fMatch.education}</p>
            </li>

            <li>
              <h5>Ethniticty</h5>
              <p>{fMatch.ethnicity}</p>
            </li>

            <li>
              <h5>Language</h5>
              <p>{fMatch.language}</p>
            </li>

            <li>
              <h5>Would they relocate?</h5>
              <p>{fMatch.relocation}</p>
            </li>
          </ul>
        </section>
      </div>
    </div>
  );
}
