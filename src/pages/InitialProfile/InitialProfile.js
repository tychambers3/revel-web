import React from 'react'
import Navbar from '../../layouts/Navbar/Navbar';
import './InitialProfile.scss';
import InitialProfileForm from '../../components/Forms/InitialProfileForm';

const InitialProfile = () => {
  
  return (
    <div className="w-100pr pos-r">
      <Navbar />

      <div className="initial-profile d-f">
        <div className="left">
          <h1>Time for a fresh profile!</h1>
        </div>

        <div className="right d-f fxd-c ai-c">
          <p className="w-504px ta-c mt+">We'd like to encourage you to fill out as much as you can. This information will help us build your profile, and help us find you some matches.</p>

          <InitialProfileForm />
        </div>
      </div>
    </div>
  )
}

export default InitialProfile;
