import React from 'react';
import './IndexPage.scss';
import { Link } from 'react-router-dom';
import Navbar from '../../layouts/Navbar/Navbar';

const IndexPage = (params) => {
  
  return (
    <div className="hero-wrapper">
      <section className="full-page-hero">
        <h1 className="hero title">Revel</h1>
        <Navbar />
      </section>
      <div className="content">
        <h1>When you want more than casual</h1>
        <p>We created Revel because we believe that finding a long-term relationship shouldn't be difficult, or break the bank. We believe by giving you all the necessary tools, we can help you find that certain someone.</p>
        <Link to="/signup" className="btn special">Join Revel</Link>
      </div>
    </div>
  );
}

export default IndexPage;