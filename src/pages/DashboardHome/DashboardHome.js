import React from 'react';
import './DashboardHome.scss';
import ProfileView from '../../components/ProfileViews/ProfileView';
import { MatchList } from '../../components/MatchList/MatchList';
import ActivityList from '../../components/ActivityList/ActivityList';

const DashboardHome = (props) => {
  return(
    <div className="d-f fxd-c dashboard-home">
      <MatchList />
      
      <div className="d-f">
        <div className="left">
          <ActivityList />
        </div>

        <div className="right">
          <ProfileView />
        </div>
      </div>
    </div>
  );
}

export default DashboardHome;

