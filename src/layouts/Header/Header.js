import React from 'react';
import './Header.scss';
import { Link } from 'react-router-dom';

export const Header = (props) => {
  return(
    <header className="header h-100p pl+ pr+">
      <h1>Revel</h1>

      <div className="d-f fxd-c">
        <h4>Main Menu</h4>
        <ul className="d-f fxd-c nav-link-list fx-1">
          <li className="nav-link d-f ai-c mb- ">
            <i className="fas fa-home mr-"></i>
            <Link to="/dashboard/home">Home</Link>
          </li>

          <li className="nav-link d-f ai-c mb- ">
            <i className="fas fa-user-circle mr-"></i>
            <Link >Profile</Link>

          </li>
        </ul>
      </div>
    </header>
  );
};
