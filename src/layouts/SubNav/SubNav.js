import React from 'react';
import './SubNav.scss';
import { Link } from 'react-router-dom';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react-lite';

const SubNav = (props) => {
  const user = props.UserStore.getUser();

  return(
    <div className="subnav d-f ai-c pl+ pr+ jc-sb">
      <div className="d-f ai-c">
        <div className="ml- mr- subnav-item">
          <i className="fas mr- fa-inbox"></i>
          Inbox
        </div>

        <div className="ml- mr- subnav-item">
          <i className="fas mr- fa-bell"></i>
          Notifications
        </div>
      </div>

      <div className="d-f ai-c">
        <Link className="btn-match mr+" to="/dashboard/matches">Look for matches</Link>

        <div className="d-f ai-c">
          <p className="mr-">{props.UserStore.getFullName()}</p>
          <div className="subnav-profile-img"></div>
        </div>
      </div>
    </div>
  );
};

export default inject('UserStore')(observer(SubNav));