import React, { useState } from 'react'
import './Navbar.scss';
import { Link } from 'react-router-dom';

const Navbar = (props) => {
  const [ extended, setExtend ] = useState(false);

  return (
    <nav className={`navbar ${extended ? "extended": ""}`} onClick={() => setExtend(!extended)}>
      <div className={`navbar-toggle ${extended ? "extended": ""}`} >
        <i className="fas fa-th-large"></i>
      </div>

      <div className={`list ${extended ? "extended": ""}`}>
        <Link to="/">Home</Link>
        <Link to="#">About</Link>
        <Link to="#">Login</Link>

      </div>
    </nav>
  )
}

export default Navbar
