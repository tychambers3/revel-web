export const religions = [
  {
    label: "Anglican",
    value: "Anglican"
  },
  {
    label: "Apostolic",
    value: "Apostolic"
  },

  {
    label: "Assembly of God",
    value: "Assembly of God"
  },
  {
    label: "Baptist",
    value: "Baptist"
  },
  {
    label: "Catholic",
    value: "Catholic"
  },
  {
    label: "Charismatic",
    value: "Charismatic"
  },
  {
    label: "Christian Reformed",
    value: "Christian Reformed"
  },
  {
    label: "Church of Christ",
    value: "Church of Christ"
  },
  {
    label: "Episcopalian",
    value: "Episcopalian"
  },
  {
    label: "Evangelical",
    value: "Evangelical"
  },
  {
    label: "Interdenominational",
    value:  "Interdenominational"
  },
  {
    label: "Lutheran",
    value: "Lutheran"
  },
  {
    label: "Messianic",
    value: "Messianic"
  },
  {
    label: "Islam",
    value:"Islam"
  },
  {
    label: "Methodist",
    value: "Methodist"
  },
  {
    label: "Nazarene",
    value: "Nazarene"
  },
  {
    label: "None",
    value: "None"
  },
  {
    label: "Non-denominational",
    value: "Non-denominational"
  },
  {
    label: "Not sure yet",
    value: "Not sure yet"
  },
  {
    label: "Orthodox",
    value: "Orthodox"
  },
  {
    label: "Pentecostal",
    value: "Pentecostal"
  },
  {
    label: "Presbyterian",
    value: "Presbyterian"
  },
  {
    label: "Seventh-Day Adventist",
    value: "Seventh-Day Adventist"
  },
  {
    label: "Southern Baptist",
    value: "Southern Baptist"
  },
  {
    label: "Other Religion",
    value: "Other Religion"
  }
];

export const education = [
  {
    label: "High School",
    value: "high school",
  }, 
  {
    label: "College",
    value: "college"
  }, 
  {
    label: "Some College",
    value: "some college"
  },
  {
    label: "Bachelor's Degree",
    value: "bachelor's degree"
  },
  {
    label: "Master's Degree",
    value: "master's degree"
  },
  {
    label: "PhD",
    value: "PhD"
  }
];

export const smoke = [
  {
    label: "Yes",
    value: "yes"
  }, 
  {
    label: "No",
    value: "no"
  }, 
  {
    label: "Sometimes",
    value: "sometimes"
  }
];

export const drink = [
  {
    label: "Yes",
    value: "yes"
  },
  {
    label: "No",
    value: "no"
  }, 
  {
    label: "Sometimes",
    value: "sometimes"
  },
  {
    label: "I drink socially",
    value: "I drink socially"
  }
];

export const relationshipType = [
  {
    label: "Long-term relationship",
    value: "long-term relationship"
  },
  {
    label: "Marriage",
    value: "Marriage"
  },
  {
    label: "Marriage with kids",
    value: "Marriage with kids"
  }
];

export const ethnicity = [
  {
    label:"Caucasian / White",
    value: "Caucasian / White"
  },
  {
    label: "African",
    value: "African"
  },
  {
    label: "Asian",
    value: "Asian"
  },
  {
    label: "Black / African descent",
    value: "Black / African descent"
  },
  {
    label: "Caribbean",
    value:  "Caribbean"
  },
  {
    label:   "East Indian",
    value:   "East Indian"
  },
  {
    label: "Hispanic / Latin",
    value: "Hispanic / Latin"
  },
  {
    label:   "Middle Eastern",
    value:   "Middle Eastern"
  },
  {
    label: "Native American",
    value: "Native American"
  },
  {
    label:   "Pacific Islander",
    value:   "Pacific Islander"
  },
  {
    label: "Other",
    value: "Other"
  }
]