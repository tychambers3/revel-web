import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './main.scss';
import IndexPage from './pages/IndexPage/IndexPage';
import SignupPage from './pages/SignupPage/SignupPage';
import InitialProfile from './pages/InitialProfile/InitialProfile';
import { DashboardPage } from './pages/DashboardPage/DashboardPage';
import { MatchShowPage } from './pages/MatchShowPage/MatchShowPage';
import { Provider } from 'mobx-react';

import UserStore from './stores/UserStore';

const stores = {
  UserStore
}

ReactDOM.render(
  <Provider {...stores}>
    <Router bsename="/">
      <Switch>
        <Route exact path="/" component={IndexPage}/>
        <Route exact path="/signup" component={SignupPage} />
        <Route exact path="/profile/initial" component={InitialProfile} />
        <Route exact path="/dashboard/:dashboard_page" component={DashboardPage} />
        <Route path="/u" component={MatchShowPage} />

      </Switch>
    </Router>
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
